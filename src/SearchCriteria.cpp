#include "include/SearchCriteria.h"

using namespace ThreadedSearch::Structure;

SearchCriteria::SearchCriteria(const SearchOptions& searchOptions)
{
    m_SearchOptions = searchOptions;
}

std::string SearchCriteria::GetSearchString() const
{
    return m_SearchOptions.SearchString;
}

bool SearchCriteria::IgnoreCase() const
{
    return m_SearchOptions.IgnoreCase;
}
