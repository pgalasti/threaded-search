#include <iostream>
#include <functional>
#include <mutex>

#include "include/typedefs.h"
#include "include/ThreadManager.h"
#include "include/AsyncTask.h"

#define RETURN_SUCCESS 0
#define RETURN_WRONG_USAGE -1

uint32 test(int num) {
    static std::mutex consoleMutex;

    std::lock_guard<std::mutex> lock(consoleMutex);
    static int count = 0;
    std::cout << num << " Thread Ran: " << ++count << std::endl;
    return num;
}

void callback() {
    std::cout << "Callback!" << std::endl;
}

int main(int argc, char** argv)
{
    ThreadedSearch::Concurrency::ThreadManager<uint32> threadPool(1000);
    ThreadedSearch::Concurrency::AsyncTask<uint32, void> task(std::bind(test, 2), std::bind(callback));


    std::function<uint32()> func = std::bind(test, 2);
    for(int i = 0; i < 1000; i++)
      threadPool.AddTask(func);
    threadPool.Start();

    threadPool.Shutdown();
    if(argc < 3)
    {
        std::cerr << "Usage: ts <directory|file> <pattern>" << std::endl;
        return RETURN_WRONG_USAGE;
    }

    return RETURN_SUCCESS;
}

