#ifndef ASYNC_TASK_H
#define ASYNC_TASK_H

#include "commons.h"

#include <functional>
#include <future>


namespace ThreadedSearch {
  namespace Concurrency {

    template<typename RetType, typename CallbackType>
    class AsyncTask
    {
    public:
       AsyncTask(const std::function<RetType()>& function, const std::function<CallbackType()>& callbackFunction)
       {
         m_Function = function;
         m_CallbackFunction = callbackFunction;
         m_IsRunning = false;
         m_ReturnedResult = false;
       };

       ~AsyncTask()
       {
         if(m_IsRunning)
             this->WaitForResult();
       };

       void Start()
       {
         if(!m_Function)
             return;

         m_Future = std::async(std::launch::async, [this]()
         {
           // Ugly
           RetType returnValue = std::async(std::launch::async, m_Function).get();
           m_CallbackFunction();
           m_IsRunning = false;
           return returnValue;
         });

         m_IsRunning = true;
       };

       RetType WaitForResult()
       {
         if(m_ReturnedResult)
           return m_Result;

         RetType result = m_Future.get();
         m_IsRunning = false;
         m_ReturnedResult = true;
         m_Result = result;

         return result;
       };

    private:
        std::future<RetType> m_Future;
        std::function<RetType()> m_Function;
        std::function<CallbackType()> m_CallbackFunction;

        bool m_IsRunning;
        bool m_ReturnedResult;
        RetType m_Result;
    };

  }
}


#endif
