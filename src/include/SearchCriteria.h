#ifndef SEARCH_CRITERIA_H
#define SEARCH_CRITERIA_H

#include <string>

#include "commons.h"


namespace ThreadedSearch {
  namespace Structure {

    struct SearchOptions
    {
      std::string SearchString;
      bool IgnoreCase;

      void Initialize()
      {
        this->SearchString = "";
        this->IgnoreCase = false;
      };
    };

    class SearchCriteria
    {
      public:
        SearchCriteria(const SearchOptions& searchOptions);

        inline std::string GetSearchString() const;
        inline bool IgnoreCase() const;

      private:
        SearchOptions m_SearchOptions;
    };
  }
}

#endif
