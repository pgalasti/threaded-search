#ifndef THREADPOOL_H
#define THREADPOOL_H

#include "commons.h"

#include <thread>
#include <future>
#include <mutex>
#include <vector>
#include <functional>
#include <map>

#include "AsyncTask.h"

namespace ThreadedSearch {
  namespace Concurrency {

    template<typename RetType>
    class ThreadManager
    {
    public:
      ThreadManager(const ushort16 limit) : m_Limit(limit), m_bRunning(false), m_NextId(0) {}

      ~ThreadManager()
      {
        this->Shutdown();
      }

      typedef AsyncTask<RetType, void> ManagedTask_t;

      void Start()
      {
        if(m_bRunning)
            return;

        m_bRunning = true;

        ushort16 numberOfTaskToAdd = 0;
        if((m_TaskMap.size()-m_ProcessingTaskMap.size()) < m_Limit)
          numberOfTaskToAdd = m_TaskMap.size();
        else
          numberOfTaskToAdd = m_Limit-m_ProcessingTaskMap.size();

        for(int i = 0; i < numberOfTaskToAdd; i++)
          this->RefreshProcess();
      }

      void Shutdown()
      {
        if(!m_bRunning)
          return;

        m_bRunning = false;

        for(auto managedTask : m_ProcessingTaskMap)
        {
          m_ProcessingTaskListMutex.lock();
          managedTask.second->WaitForResult();
          m_ProcessingTaskListMutex.unlock();
          this->RemoveTask(managedTask.first);
        }
      }

      void AddTask(const std::function<RetType()>& func)
      {
        std::function<void()> callbackFunction = std::bind(&ThreadManager::OnTaskFinishCallback, this, m_NextId);

        ManagedTask_t* pTask = new ManagedTask_t(func, callbackFunction);

        m_TaskMap[m_NextId++] = pTask;
      }

    private:

      typedef std::vector<AsyncTask<RetType, void>*> TaskList_t;
      typedef std::map<uint32, ManagedTask_t*> TaskMap_t;

      void RefreshProcess()
      {
        std::lock_guard<std::mutex> listLock(m_TaskListMutex);
        if(m_TaskMap.empty())
          return;

        std::lock_guard<std::mutex> processingLock(m_ProcessingTaskListMutex);

        ManagedTask_t* pTask = m_TaskMap.begin()->second;
        m_ProcessingTaskMap[m_TaskMap.begin()->first] = pTask;
        m_TaskMap.erase(m_TaskMap.begin()->first);
        pTask->Start();
      }

      void OnTaskFinishCallback(uint32 finishedId)
      {
        if(!m_bRunning)
            return;

        this->RemoveTask(finishedId);

        this->RefreshProcess();
      };

      void RemoveTask(uint32 id)
      {
//        std::lock_guard<std::mutex> processingLock(m_ProcessingTaskListMutex);
//        m_ProcessingTaskMap.erase(id);
      }

      ushort16 m_Limit;
      bool m_bRunning;
      ushort16 m_NextId;

      TaskMap_t m_TaskMap;
      std::mutex m_TaskListMutex;

      TaskMap_t m_ProcessingTaskMap;
      std::mutex m_ProcessingTaskListMutex;
    };

  }
}


#endif
