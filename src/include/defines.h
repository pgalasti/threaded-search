#ifndef DEFINES_H
#define DEFINES_H

#ifdef __unix__

#define NEW_LINE "\n"
#define FILE_SEPARATOR '/'

#elif _WIN32

#define NEW_LINE "\r\n"
#define FILE_SEPARATOR '\\'

#endif



#endif
